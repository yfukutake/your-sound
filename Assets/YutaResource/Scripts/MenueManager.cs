﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Microsoft.MixedReality.Toolkit.UI;

public class MenueManager : MonoBehaviour
{

    public event Action onMusicPressed;
    public event Action onGamePressed;

    [SerializeField] FollowMeToggle[] followers;

    Transform mixedRealityPlayspace;
    Vector3 startingPosition;
    Quaternion startingAngle;


    void Start()
    {
        mixedRealityPlayspace = GameObject.FindGameObjectsWithTag("Player")[0].transform;
        startingPosition = mixedRealityPlayspace.position;
        startingAngle = mixedRealityPlayspace.rotation;

    }

    public void BackStartingPoint() 
    {
        if (mixedRealityPlayspace.position == startingPosition) { return; }
        mixedRealityPlayspace.position = startingPosition;
        mixedRealityPlayspace.rotation = startingAngle;
        
    }
    public void ExitGame() 
    {
        System.Diagnostics.Process.GetCurrentProcess().Kill();
    }

    public void MusicPressed() 
    {
        onMusicPressed += SceneLoader.sceneLoader.LoadMusic;
        onMusicPressed();
        
    }
    public void GamePressed() 
    {
        onGamePressed += SceneLoader.sceneLoader.LoadGame;
        onGamePressed();
        
    }

    public void FollowMeBehaviorAlways() 
    {
        foreach (FollowMeToggle follower in followers) 
        {
            follower.SetFollowMeBehavior(true);
        }
    }

    /*private void Update()
    {
        if (Input.GetKeyDown(KeyCode.M)) 
        {
            MusicPressed();
        }
        if (Input.GetKeyDown(KeyCode.G)) 
        {
            GamePressed();
        }

    }*/


}
