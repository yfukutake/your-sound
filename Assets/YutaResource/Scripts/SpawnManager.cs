﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Microsoft.MixedReality.Toolkit.UI;

public class SpawnManager : MonoBehaviour, ISaveable
{
    public float scale;
    public float spawnSpeed;

    [SerializeField] GameObject[] spawnables;
    [SerializeField] GameObject spawnedParentTransform;
    [SerializeField] GameObject spawner;

    Transform spawnCircle, spawnPoint, spawnLookAtTarget;
    Collider[] spawnerColliders;
 

    void Start()
    {
        spawnCircle = spawner.transform.GetChild(0);
        spawnPoint = spawnCircle.GetChild(2);
        spawnLookAtTarget = spawnCircle.GetChild(3);
        spawnerColliders = spawner.GetComponentsInChildren<Collider>();
    }

    public void PresedTriggerSpawn(int index) 
    {

        GameObject spawnedObj = Instantiate(spawnables[index], GetSpawnPoint(), Quaternion.identity);

        spawnedObj.transform.parent = spawnedParentTransform.transform;
        spawnedObj.GetComponent<Rigidbody>().AddForce(Vector3.forward * spawnSpeed, ForceMode.Impulse);
        spawnedObj.transform.localScale = Vector3.one * scale;
        spawnedObj.transform.GetChild(0).localScale = Vector3.one * scale;
        
    }

    public void ShootFromSpawner(int index) 
    {

        GameObject spawnedObj = Instantiate(spawnables[index], spawnPoint.position, Quaternion.identity);

        Collider spawnableCollider = spawnedObj.GetComponent<Collider>();
        foreach (Collider spawner in spawnerColliders) 
        {
            Physics.IgnoreCollision(spawnableCollider, spawner);
        }
        
        spawnedObj.transform.parent = spawnedParentTransform.transform;

        Vector3 relativeLocalDirection = spawnLookAtTarget.position - spawnPoint.position;
        spawnedObj.GetComponent<Rigidbody>().AddRelativeForce(relativeLocalDirection * spawnSpeed, ForceMode.Impulse);

        Vector3 spawnerScale = spawnCircle.localScale;
        Vector3 sliderScale = Vector3.one * scale;
        if (spawnerScale.sqrMagnitude > sliderScale.sqrMagnitude)
        {
            spawnedObj.transform.localScale = spawnerScale;
            spawnedObj.transform.GetChild(0).localScale = spawnerScale;
        }
        else if (spawnerScale.sqrMagnitude < sliderScale.sqrMagnitude) 
        {
            spawnedObj.transform.localScale = sliderScale;
            spawnedObj.transform.GetChild(0).localScale = sliderScale;
        }
    }

    private Vector3 GetSpawnPoint() 
    {
        Vector3 spawnPoint = new Vector3(Random.Range(-6f, 8f), Random.Range(0f, 14f), 1f);
        return spawnPoint;
    }

    public void InitializeOnStart()
    {
        spawnCircle = spawner.transform.GetChild(0);
        spawnPoint = spawnCircle.GetChild(2);
        spawnLookAtTarget = spawnCircle.GetChild(3);
        spawnerColliders = spawner.GetComponentsInChildren<Collider>();

    }

    public object CaptureState()
    {
        
        float[] spawnNums = {scale, spawnSpeed};
        return spawnNums;
    }

    public void RestoreState(object state)
    {
        if (state == null) { return; }
        float[] restore = state as float[];
        if (restore == null) 
        {
            Debug.LogError("Spawn Numbers are empty");
            return;
        }
        scale = restore[0];
        spawnSpeed = restore[1];
    }
}
