﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnedObject : MonoBehaviour
{
    [SerializeField] float timeBeforeDestroy;

    MeshRenderer spawnableMesh;
    Rigidbody spawnableRigibody;
    ParticleSystem particle;
    AudioSource audioSource;
    
    
    void Start()
    {
        particle = GetComponentInChildren<ParticleSystem>();
        audioSource = GetComponentInChildren<AudioSource>();
        spawnableMesh = GetComponent<MeshRenderer>();
        spawnableRigibody = GetComponent<Rigidbody>();
        
    }

    public Coroutine TriggerExplosion() 
    {
        return StartCoroutine(OnDestroyBehavior());
    }

    IEnumerator OnDestroyBehavior() 
    {
        spawnableMesh.enabled = false;
        spawnableRigibody.velocity = Vector3.zero;
        particle.Play();

        audioSource.Play();

        while (particle.isPlaying) 
        {
            yield return null;
        }
        audioSource.Stop();

        Destroy(this.gameObject, timeBeforeDestroy);
    }

    public void OnPressedDestroy()
    {
        StartCoroutine(OnDestroyBehavior());
    }
}
