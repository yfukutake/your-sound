﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Microsoft.MixedReality.Toolkit.UI;

public class PositionManager : MonoBehaviour, ISaveable
{
    [SerializeField] private Transform targetPosition;

    [SerializeField] private Vector3 startingPosition;

    private float offsetX, offsetY, offsetZ;
    private bool onStartX, onStartY, onStartZ;
    private void Start()
    {
       
    }
  
    public void OnSliderUpdatePositionX(SliderEventData eventData)
    {
        if (onStartX) 
        {
            CalculateOffsetX(eventData);
            onStartX = false;
        }
        if (targetPosition == null) { return; }
        targetPosition.position = new Vector3(eventData.NewValue*offsetX, targetPosition.position.y, targetPosition.position.z);
    }

    public void OnSliderUpdatePositionY(SliderEventData eventData) 
    {
        if (onStartY) 
        {
            CalculateOffsetY(eventData);
            onStartY = false;
        }
        if (targetPosition == null) { return; }
        targetPosition.position = new Vector3(targetPosition.position.x, eventData.NewValue*offsetY, targetPosition.position.z);
    }

    public void OnSliderUpdatePositionZ(SliderEventData eventData)
    {
        if (onStartZ) 
        {
            CalculateOffsetZ(eventData);
            onStartZ = false;
            
        }
        if (targetPosition == null) { return; }
        targetPosition.position = new Vector3(targetPosition.position.x, targetPosition.position.y, eventData.NewValue*offsetZ); ;
    }

    private void CalculateOffsetX(SliderEventData eventData)
    {
        offsetX = startingPosition.x / eventData.NewValue;
    }
    private void CalculateOffsetY(SliderEventData eventData) 
    {
        offsetY = startingPosition.y / eventData.NewValue;
    }

    private void CalculateOffsetZ(SliderEventData eventData) 
    {
        offsetZ = startingPosition.z / eventData.NewValue;
    }

    public void InitializeOnStart() 
    {
        if (targetPosition != null)
        {
            targetPosition.position = startingPosition;
        }
        onStartX = true;
        onStartY = true;
        onStartZ = true;
    }
    public object CaptureState() 
    {
        return targetPosition.position;
       
    }
    public void RestoreState(object state) 
    {
        targetPosition.position = (Vector3)state;

    }
}
