﻿public interface ISaveable 
{
    void InitializeOnStart();
    object CaptureState();
    void RestoreState(object state);
}