﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
    public static SceneLoader sceneLoader;
    public Dictionary<string, Dictionary<string, object>> masterDictionary = new Dictionary<string, Dictionary<string, object>>();

    [SerializeField] OVROverlay overlayBackground;
    [SerializeField] OVROverlay overlayText;
    private void Awake()
    {
        if (sceneLoader != null && sceneLoader != this) 
        {
            Destroy(this.gameObject);
            return;
        }
        sceneLoader = this;
        DontDestroyOnLoad(gameObject);
    }
    public void LoadScene(string sceneName) 
    {
        StartCoroutine(ShowOverlayOnLoad(sceneName));
        
    }
    IEnumerator ShowOverlayOnLoad(string sceneName) 
    {
        overlayBackground.enabled = true;
        overlayText.enabled = true;
        Vector3 playerPos = GameObject.FindGameObjectsWithTag("Player")[0].transform.position;
        overlayText.transform.position = playerPos + new Vector3(0, 1.2f, 3f);

        yield return new WaitForSeconds(3f); //time for capture state

        yield return SceneManager.LoadSceneAsync(sceneName);

        yield return new WaitForSeconds(3f); // time for initialization on scene start

        overlayBackground.enabled = false;
        overlayText.enabled = false;
    }
    public void LoadMusic() 
    {
        string currentScene = SceneManager.GetActiveScene().name;
        if (currentScene == "Music") 
        {
            Debug.Log("you are on current Music scene");
            return;
        }
        LoadScene("Music");
    }
    public void LoadGame() 
    {
        string currentScene = SceneManager.GetActiveScene().name;
        if (currentScene == "Game") 
        {
            Debug.Log("You are on current Game scene");
            return; 
        }
        LoadScene("Game");
    }
}
