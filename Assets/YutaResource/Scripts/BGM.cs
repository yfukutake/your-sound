﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BGM : MonoBehaviour
{
    AudioSource _audioSource;
    bool isToggled = false;
    public void MusicStart() 
    {
        if (!isToggled)
        {
            _audioSource = GetComponent<AudioSource>();
            _audioSource.Play();
            isToggled = true;
            return;
        }
        else if (isToggled) 
        {
            _audioSource = GetComponent<AudioSource>();
            _audioSource.Stop();
            isToggled = false;
            return;
        }
    }

}
