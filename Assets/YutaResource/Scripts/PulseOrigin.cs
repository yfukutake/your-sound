﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PulseOrigin : MonoBehaviour, ISaveable
{
    PulseController pc;
    int pulseOriginID;

    public void InitializeOnStart()
    {
        pc = GameObject.FindWithTag("Pulse").GetComponent<PulseController>();
        pulseOriginID = Shader.PropertyToID("_Pulse_Origin_");
    }

    public object CaptureState()
    {
        Vector4 pulseOrigin;
        return pulseOrigin = pc.PurpleMusic.GetVector(pulseOriginID);

    }

    public void RestoreState(object state)
    {
        Vector4 restore = (Vector4)state;
        pc.PurpleMusic.SetVector(pulseOriginID, restore);
    }


}
