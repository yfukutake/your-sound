﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RadialButtonManager : MonoBehaviour
{
    [SerializeField] Sliders[] spawnables;
    [SerializeField] Sliders[] pulseables;
    
    PulseController pulseController;
    
    void Start()
    {
        pulseController = GameObject.FindWithTag("Pulse").GetComponent<PulseController>();
    }

    public void TrueSpawning()
    {
        MakeTrue(spawnables);
    }
    public void FalseSpawning()
    {
        MakeFalse(spawnables);
    }

    public void TruePulse() 
    {
        MakeTrue(pulseables);
    }
    public void FalsePulse() 
    {
        MakeFalse(pulseables);
    }

    public void TurnBlackMatTrue(bool _isBlack) 
    {
        pulseController.isBlack = _isBlack;
    }

    [SerializeField] GameObject gameFieldForEnableSpatial, gameFieldForEnableHand; 
    public void EnableField(bool spatial)
    {
        if (spatial)
        {
            ActivateField(gameFieldForEnableSpatial);
        }
        else
        {
            ActivateField(gameFieldForEnableHand);
        }

    }
    void ActivateField(GameObject field)
    {
        if (field.activeInHierarchy)
        {
            field.SetActive(false);
        }
        else if (!field.activeInHierarchy)
        {
            field.SetActive(true);
        }
    }


    private void MakeTrue(Sliders[] sliders) 
    {
        for (int i = 0; i < sliders.Length; i++) 
        {
            sliders[i].isSelectedValue = true;
        }
    }
    private void MakeFalse(Sliders[] sliders)
    {
        for (int i = 0; i < sliders.Length; i++)
        {
            sliders[i].isSelectedValue = false;
        }
    }
    
}
