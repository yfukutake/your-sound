﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveableEntity : MonoBehaviour
{
    ISaveable[] saveables;
    Dictionary<string, object> sceneDict;
    string stageName;
    MenueManager menueManager;
    
    IEnumerator Start()
    {
        saveables = GetComponentsInChildren<ISaveable>();

        stageName = UnityEngine.SceneManagement.SceneManager.GetActiveScene().name;

        foreach (ISaveable saveable in saveables) 
        {
            saveable.InitializeOnStart();
        }

        yield return null;

        if (SceneLoader.sceneLoader.masterDictionary.ContainsKey(stageName)) 
        {
            yield return null; // avoid race condition
            RestoreState();
        }

        menueManager = GetComponentInChildren<MenueManager>();
        menueManager.onGamePressed += CaptureState;
        menueManager.onMusicPressed += CaptureState;
    }


    public void CaptureState() 
    {
        sceneDict = new Dictionary<string, object>();

        foreach (ISaveable saveable in saveables) 
        {
            string name = saveable.GetType().ToString();

            sceneDict[name] = saveable.CaptureState();
        }

        SceneLoader.sceneLoader.masterDictionary[stageName] = sceneDict;
       
    }
    public void RestoreState() 
    {
        Dictionary<string, object> restoreDict = SceneLoader.sceneLoader.masterDictionary[stageName];

        foreach (ISaveable saveable in saveables) 
        {
            string name = saveable.GetType().ToString();
            object state = restoreDict[name];
            saveable.RestoreState(state);
        }
        
    }
}
